import { Controller, Get, Post, Body, ValidationPipe, Put, Delete, ParseIntPipe, Query } from '@nestjs/common';
import { EventDTO } from './models/event.dto';
import { EventsService } from './events.service';
import { CreateEventDTO } from './models/createEvent.dto';
import { EditEventDTO } from './models/editEvent.dto';

@Controller('events')
export class EventsController {

  constructor(private readonly eventsService: EventsService) {

  }

  @Get()
  public async getEvents(): Promise<EventDTO[]> {
    return await this.eventsService.getEvents();
  }

  @Post()
  public async createEvent(@Body(new ValidationPipe({ transform: true, whitelist: true})) event: CreateEventDTO): Promise<EventDTO> {
    return await this.eventsService.createEvent(event);
  }

  @Put()
  public async editEvent(@Body(new ValidationPipe({ transform: true, whitelist: true})) event: EditEventDTO): Promise<EventDTO> {
    return await this.eventsService.editEvent(event);
  }

  @Delete()
  public async deleteEvent(@Query('id', new ParseIntPipe()) eventId: number): Promise<{ msg: string }> {
    await this.eventsService.deleteEvent(eventId);
    return { msg: 'Deleted succesfuly' };
  }
}
