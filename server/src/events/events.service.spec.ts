import { Test, TestingModule } from '@nestjs/testing';
import { EventsService } from './events.service';
import * as fs from 'fs';
import { FsService } from '../fs.service';
import { async } from 'rxjs/internal/scheduler/async';

describe('EventsService', () => {
  let service: EventsService;
  const fsService = {
    readFromFile() { /* empty */ },
    writeFromFile() { /* empty */ },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventsService,
        {
          provide: FsService,
          useValue: fsService,
        },
      ],
    }).compile();

    service = module.get<EventsService>(EventsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getEvents', () => {
    it('should call fsService.readFromFile with correct parameters', async () => {

      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => ({ events: [] }));

      await service.getEvents();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(service.eventsFilePath);
    });

    it('should data from fsService.readFromFile', async () => {

      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => ({ events: 'test' }));

      const result = await service.getEvents();

      expect(result).toEqual('test');
    });
  });

  describe('createEvent', () => {
    it('should call fsService.readFromFile with correct parameters', async () => {
      const event = {};
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => ({ events: [] }));

      await service.createEvent(event);

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(service.eventsFilePath);
    });

    it('should set event id', async () => {
      const event = {};
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => ({ events: [] }));

      const result = await service.createEvent(event);

      expect(result.id).toEqual(service.idCounter - 1);
    });

    it('should set date', async () => {
      const event = {};
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => ({ events: [] }));

      const result = await service.createEvent(event);

      expect(result.eventStartDate).toEqual(new Date(new Date().setHours(23, 59)).toLocaleString());
    });

    it('should push to events array', async () => {
      const event = {};
      const eventsJson = { events: [] };
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);

      const result = await service.createEvent(event);

      expect(eventsJson.events).toHaveLength(1);
    });

    it('should call fsService.writeFromFile with correct parameters', async () => {
      const event = {};
      const eventsJson = { events: [] };
      jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);
      const spy = jest.spyOn(fsService, 'writeFromFile');

      await service.createEvent(event);

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(service.eventsFilePath, eventsJson);
    });

    it('should return correct data', async () => {
      const event = {};
      const eventsJson = { events: [] };
      jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);
      const spy = jest.spyOn(fsService, 'writeFromFile');

      const result = await service.createEvent(event);

      expect(result).toBe(event);
    });
  });

  describe('editEvent', () => {
    it('should call fsService.readFromFile with correct parameters', async () => {
      const event = {};
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => ({ events: [] }));

      await service.editEvent(event as any);

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(service.eventsFilePath);
    });

    it('should update the event with updated event', async () => {
      const event = { id: 2, name: 'updated' };
      const eventsJson = { events: [{ id: 2, name: 'test' }] };
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);

      await service.editEvent(event as any);

      expect(eventsJson.events[0].name).toEqual('updated');
    });

    it('should call fsService.writeFromFile with correct parameters', async () => {
      const event = {};
      const eventsJson = { events: [] };
      jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);
      const spy = jest.spyOn(fsService, 'writeFromFile');

      await service.editEvent(event as any);

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(service.eventsFilePath, eventsJson);
    });

    it('should return correct data', async () => {
      const event = {};
      const eventsJson = { events: [] };
      jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);
      const spy = jest.spyOn(fsService, 'writeFromFile')
        .mockImplementation(() => 'test');

      const result = await service.editEvent(event as any);

      expect(result).toBe(event);
    });
  });

  describe('deleteEvent', () => {
    it('should call fsService.readFromFile with correct parameters', async () => {
      const event = {};
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => ({ events: [] }));

      await service.deleteEvent(event as any);

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(service.eventsFilePath);
    });

    it('should remove event with give id', async () => {
      const eventId = 2;
      const eventsJson = { events: [{ id: 2, name: 'test' }] };
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);

      await service.deleteEvent(eventId);

      expect(eventsJson.events).toHaveLength(0);
    });

    it('should not find element to remove', async () => {
      const eventId = 1;
      const eventsJson = { events: [{ id: 2, name: 'test' }] };
      const spy = jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);

      await service.deleteEvent(eventId);

      expect(eventsJson.events).toHaveLength(1);
    });

    it('should call fsService.writeFromFile with correct parameters', async () => {
      const event = {};
      const eventsJson = { events: [] };
      jest.spyOn(fsService, 'readFromFile')
        .mockImplementation(() => eventsJson);
      const spy = jest.spyOn(fsService, 'writeFromFile');

      await service.deleteEvent(event as any);

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(service.eventsFilePath, eventsJson);
    });
  });
});
