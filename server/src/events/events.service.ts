import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import { EventDTO } from './models/event.dto';
import { FsService } from '../fs.service';

@Injectable()
export class EventsService {

  idCounter = 5;
  eventsFilePath = './src/db/events.json';

  constructor(private readonly fsService: FsService) {}

  async getEvents(): Promise<EventDTO[]> {
    return (await this.fsService.readFromFile(this.eventsFilePath)).events;
  }

  async createEvent(event: Partial<EventDTO>): Promise<EventDTO> {
    const eventsJson = await this.fsService.readFromFile(this.eventsFilePath);

    event.id = this.idCounter++;
    event.eventStartDate = new Date(new Date().setHours(23, 59)).toLocaleString();
    eventsJson.events.push(event as EventDTO);

    this.fsService.writeFromFile(this.eventsFilePath, eventsJson);

    return event as EventDTO;
  }

  async editEvent(event: EventDTO): Promise<EventDTO> {
    const eventsJson = await this.fsService.readFromFile(this.eventsFilePath);

    eventsJson.events = eventsJson.events.map((x) => x.id === event.id ? event : x);

    await this.fsService.writeFromFile(this.eventsFilePath, eventsJson);

    return event as EventDTO;
  }

  async deleteEvent(eventId: number): Promise<void> {
    const eventsJson = await this.fsService.readFromFile(this.eventsFilePath);

    eventsJson.events = eventsJson.events.filter((x) => x.id !== eventId);

    await this.fsService.writeFromFile(this.eventsFilePath, eventsJson);
  }
}
