import { IsOptional, IsString, IsNumber, Min, Length } from 'class-validator';

export class CreateEventDTO {
  @IsOptional()
  @IsString()
  @Length(3, 50)
  eventName: string;

  @IsOptional()
  @Min(1)
  @IsNumber({ maxDecimalPlaces: 2 })
  oddsForFirstTeam: number;

  @IsOptional()
  @Min(1)
  @IsNumber({ maxDecimalPlaces: 2 })
  oddsForDraw: number;

  @IsOptional()
  @Min(1)
  @IsNumber({ maxDecimalPlaces: 2 })
  oddsForSecondTeam: number;
}
