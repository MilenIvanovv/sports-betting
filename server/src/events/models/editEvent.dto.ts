import { IsOptional, IsString, IsNumber, IsNotEmpty, Min, Length, IsDate } from 'class-validator';

export class EditEventDTO {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsOptional()
  @IsString()
  @Length(3, 50)
  eventName: string;

  @IsOptional()
  @Min(1)
  @IsNumber({ maxDecimalPlaces: 2 })
  oddsForFirstTeam: number;

  @IsOptional()
  @Min(1)
  @IsNumber({ maxDecimalPlaces: 2 })
  oddsForDraw: number;

  @IsOptional()
  @Min(1)
  @IsNumber({ maxDecimalPlaces: 2 })
  oddsForSecondTeam: number;

  @IsOptional()
  @IsString()
  @Length(16, 50)
  eventStartDate: string;
}
