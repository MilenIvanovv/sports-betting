export class EventDTO {
  id: number;
  eventName: string;
  oddsForFirstTeam: number;
  oddsForDraw: number;
  oddsForSecondTeam: number;
  eventStartDate: string;
}
