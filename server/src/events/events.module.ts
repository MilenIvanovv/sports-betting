import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import * as fs from 'fs';
import { FsService } from '../fs.service';

@Module({
  providers: [
    EventsService,
    FsService,
  ],
  controllers: [EventsController],
})
export class EventsModule {}
