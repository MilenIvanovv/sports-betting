import { Test, TestingModule } from '@nestjs/testing';
import { EventsController } from './events.controller';
import { EventsService } from './events.service';

describe('Events Controller', () => {
  let controller: EventsController;
  const eventsService = {
    getEvents() { /* empty */ },
    createEvent() {/* empty */ },
    editEvent() {/* empty */ },
    deleteEvent() {/* empty */ },
  };

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      controllers: [EventsController],
      providers: [
        {
          provide: EventsService,
          useValue: eventsService,
        },
      ],
    }).compile();

    controller = module.get<EventsController>(EventsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('getEvents should call eventsService.getEvents', async () => {

    const spy = jest.spyOn(eventsService, 'getEvents');

    await controller.getEvents();

    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('getEvents should return the result of eventsService.getEvents', async () => {

    const spy = jest.spyOn(eventsService, 'getEvents')
      .mockImplementation(async () => 'test');

    const result = await controller.getEvents();

    expect(result).toEqual('test');
  });

  it('createEvent should call eventsService.createEvent', async () => {
    const event = {};
    const spy = jest.spyOn(eventsService, 'createEvent');

    await controller.createEvent(event as any);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(event);
  });

  it('createEvent should return the result of eventsService.createEvent', async () => {
    const event = {};
    const spy = jest.spyOn(eventsService, 'createEvent')
      .mockImplementation(async () => 'test');

    const result = await controller.createEvent(event as any);

    expect(result).toEqual('test');
  });

  it('editEvent should call eventsService.editEvent', async () => {
    const event = {};
    const spy = jest.spyOn(eventsService, 'editEvent');

    await controller.editEvent(event as any);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(event);
  });

  it('editEvent should return the result of eventsService.editEvent', async () => {
    const event = {};
    const spy = jest.spyOn(eventsService, 'editEvent')
      .mockImplementation(async () => 'test');

    const result = await controller.editEvent(event as any);

    expect(result).toEqual('test');
  });

  it('deleteEvent should call eventsService.deleteEvent', async () => {
    const event = {};
    const spy = jest.spyOn(eventsService, 'deleteEvent');

    await controller.deleteEvent(event as any);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(event);
  });

  it('deleteEvent should return msg', async () => {
    const event = {};

    const result = await controller.deleteEvent(event as any);

    expect(result).toEqual({ msg: 'Deleted succesfuly' });
  });
});
