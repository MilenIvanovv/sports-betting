import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import { EventDTO } from './events/models/event.dto';

@Injectable()
export class FsService {
  async readFromFile(path): Promise<{ events: EventDTO[] }> {
    return await new Promise<{ events: EventDTO[] }>((res, rej) => {
      fs.readFile(path, (err, data) => {
        if (err) {
          throw err;
        }

        res(JSON.parse(data as any));
      });
    });
  }

  async writeFromFile(path, data): Promise<void> {
    await new Promise<{ events: EventDTO[] }>((res, rej) => {
      fs.writeFile(path, JSON.stringify(data), (err) => {
        if (err) {
          throw err;
        }
        res();
      });
    });
  }
}
