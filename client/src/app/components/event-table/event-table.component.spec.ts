import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventTableComponent } from './event-table.component';
import { EventRowComponent } from '../event-row/event-row.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from '../../core/core.module';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../../app.component';
import { EventsService } from '../../core/events.service';
import { of } from 'rxjs';

describe('EventTableComponent', () => {
  let component: EventTableComponent;
  let fixture: ComponentFixture<EventTableComponent>;

  const eventsService = {
    getAllEvents() {

    },

    createNewEvent() {

    }
  };

  beforeEach(async(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        NgbModule,
        HttpClientModule,
        CoreModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        EventTableComponent,
        EventRowComponent
      ],
      providers: [
        EventsService,
      ]
    })
      .overrideProvider(EventsService, { useValue: eventsService })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventTableComponent);
    component = fixture.componentInstance;
  });

  describe('ngOnInit should', () => {
    it('call eventsService getAllEvents once and with correct parameters', () => {
      const spy = jest.spyOn(eventsService, 'getAllEvents')
        .mockImplementation(() => of());

      component.ngOnInit();

      expect(spy).toHaveBeenCalled();
    });

    it('get all events', () => {
      const data = [];

      const spy = jest.spyOn(eventsService, 'getAllEvents')
        .mockImplementation(() => of(data));

      component.ngOnInit();

      expect(component.events).toBe(data);
    });
  });

  describe('addNewEventHandler should', () => {
    it('call eventsService createNewEvent once', () => {
      const data = [];

      const spy = jest.spyOn(eventsService, 'createNewEvent')
        .mockImplementation(() => of(data));

      component.addNewEventHandler();

      expect(spy).toHaveBeenCalled();
    });

    it('add new event', () => {
      const data = [];

      const spy = jest.spyOn(eventsService, 'getAllEvents')
        .mockImplementation(() => of(data));

      component.addNewEventHandler();

      expect(component.events).toHaveLength(1);
    });
  });

  describe('deleteEventHandler should', () => {
    it('remove event from data', () => {
      const event = {id: 2};
      const data = [{ id: 2 }];

      component.events = data;

      component.deleteEventHandler(event.id);

      expect(component.events).toHaveLength(0);
    });
  });

  it('not remove event from data', () => {
    const event = {id: 3};
    const data = [{ id: 2 }];

    component.events = data;

    component.deleteEventHandler(event.id);

    expect(component.events).toHaveLength(1);
  });
});
