import { Component, OnInit } from '@angular/core';
import { EventsService } from '../../core/events.service';

@Component({
  selector: 'app-event-table',
  templateUrl: './event-table.component.html',
  styleUrls: ['./event-table.component.css']
})
export class EventTableComponent implements OnInit {

  events = [];
  canEditTable = false;

  constructor(private readonly eventsService: EventsService) {}

  ngOnInit() {
    this.eventsService.getAllEvents()
      .subscribe((data) => this.events = data);
  }

  addNewEventHandler() {
    this.eventsService.createNewEvent({})
      .subscribe((data) => this.events.push(data));
  }

  deleteEventHandler(eventId) {
    this.events = this.events.filter((event) => event.id !== eventId);
  }
}
