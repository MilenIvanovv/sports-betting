import { Component, Input, OnInit, Output, EventEmitter, OnChanges, HostBinding } from '@angular/core';
import { SportEvent } from '../../common/event.interface';
import { EventsService } from '../../core/events.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[event-row]',
  templateUrl: './event-row.component.html',
  styleUrls: ['./event-row.component.css']
})
export class EventRowComponent implements OnInit, OnChanges {

  @Input() event: SportEvent;
  @Input() canEditTable: boolean;
  @Output() deleteEvent = new EventEmitter();
  canEditRow = false;
  isDatePassed = false;

  // alternatively also the host parameter in the @Component()` decorator can be used
  @HostBinding('class.passed-date') colorPassedDate = false;

  constructor(private readonly eventsService: EventsService) {}

  ngOnInit() {
    this.isDatePassed = this.getIsDatePassed(this.event.eventStartDate);
    this.colorPassedDate = this.isDatePassed;
  }

  ngOnChanges() {
    if (!this.canEditTable) {
      this.colorPassedDate = this.isDatePassed;
    } else {
      this.colorPassedDate = false;
    }
  }

  editTableHandler() {
    this.eventsService.editEvent(this.event).subscribe((data) => {
      this.isDatePassed = this.getIsDatePassed(this.event.eventStartDate);
      this.canEditRow = false;
    }, () => {});
  }

  deleteEventHandler() {
    this.eventsService.deleteEvent(this.event.id).subscribe((data) => {
      this.deleteEvent.emit(this.event.id);
    }, () => {});
  }

  OddClickHandler(ev) {
    if (this.canEditTable) {
      return;
    }

    const cellHeader = ev.target.parentNode.parentNode.parentNode.tHead.rows[0].cells[ev.target.cellIndex].innerHTML;
    const oddKey = cellHeader.charAt(0).toLowerCase() + cellHeader.slice(1);

    console.log(`${this.event.id} ${cellHeader} ${this.event[oddKey]}`);
  }

  private getIsDatePassed(date) {
    return new Date() > new Date(date);
  }
}
