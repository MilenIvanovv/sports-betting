import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventRowComponent } from './event-row.component';
import { EventsService } from '../../core/events.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from '../../core/core.module';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../../app.component';
import { EventTableComponent } from '../event-table/event-table.component';
import { of } from 'rxjs';

describe('EventRowComponent', () => {
  let component: EventRowComponent;
  let fixture: ComponentFixture<EventRowComponent>;

  const eventsService = {
    editEvent(event) {

    },

    deleteEvent(id) {

    }
  };

  beforeEach(async(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        NgbModule,
        HttpClientModule,
        CoreModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        EventTableComponent,
        EventRowComponent
       ],
      providers: [
        EventsService,
      ]
    })
    .overrideProvider(EventsService, { useValue: eventsService })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventRowComponent);
    component = fixture.componentInstance;
  });

  it('should be defined', () => {
    (component.event as any) = { eventStartDate: ''};
    fixture.detectChanges();

    expect(component).toBeDefined();
  });

  describe('ngOnInit should', () => {
    it('compare passed event date with the date today', () => {
      (component.event as any) = { eventStartDate: '1.1.1997'};

      component.ngOnInit();

      expect(component.isDatePassed).toBeTruthy();
    });

    it('compare future event date with the date today', () => {
      (component.event as any) = { eventStartDate: '1.1.2025'};

      component.ngOnInit();

      expect(component.isDatePassed).toBeFalsy();
    });

    it('colorPassedDate be equal to isDatePassed', () => {
      (component.event as any) = { eventStartDate: '1.1.1997'};

      component.ngOnInit();

      expect(component.colorPassedDate).toBeTruthy();
    });
  });

  describe('ngOnChanges should', () => {
    it('color passed events when you switch to preview mode', () => {
      (component.event as any) = { eventStartDate: '1.1.1997' };
      component.canEditTable = false;

      component.ngOnChanges();

      expect(component.isDatePassed).toEqual(component.canEditTable);
    });

    it('remove color on passed events when you switch to edit mode', () => {
      (component.event as any) = { eventStartDate: '1.1.1997'};
      component.canEditTable = true;

      component.ngOnChanges();

      expect(component.colorPassedDate).toBeFalsy();
    });
  });

  describe('editTableHandler should', () => {
    it('call eventsService editEvent with event', () => {
      const event = { eventStartDate: '1.1.1997' };
      (component.event as any) = event;

      const spy = jest.spyOn(eventsService, 'editEvent')
        .mockImplementation(() => of());

      component.editTableHandler();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(event);
    });

    it('set edit mode to false', () => {
      const event = { eventStartDate: '1.1.1997' };
      (component.event as any) = event;

      const spy = jest.spyOn(eventsService, 'editEvent')
        .mockImplementation(() => of());

      component.editTableHandler();

      expect(component.canEditTable).toBeFalsy();
    });
  });

  describe('deleteEventHandler should', () => {
    it('call eventsService delete with event id', () => {
      const event = { id: 3, eventStartDate: '1.1.1997' };
      (component.event as any) = event;

      const spy = jest.spyOn(eventsService, 'deleteEvent')
        .mockImplementation(() => of());

      component.deleteEventHandler();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(event.id);
    });
  });
});
