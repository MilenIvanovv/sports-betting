import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EventTableComponent } from './components/event-table/event-table.component';
import { EventRowComponent } from './components/event-row/event-row.component';
import { CoreModule } from './core/core.module';


@NgModule({
  declarations: [
    AppComponent,
    EventTableComponent,
    EventRowComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    CoreModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
