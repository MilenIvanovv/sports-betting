import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsService } from './events.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    EventsService
  ]
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      return parent;
    }
  }
 }
