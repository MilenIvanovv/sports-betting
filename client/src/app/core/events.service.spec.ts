import { TestBed, async } from '@angular/core/testing';

import { EventsService } from './events.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CONFIG } from '../common/config';

describe('EventsService', () => {
  let service: EventsService;

  let http;

  beforeEach( async( () => {
    // clear all spies and mocks
    jest.clearAllMocks();

    http = {
      get() { },
      put() { },
      post() { },
      delete() { },
    };

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        EventsService,
      ]
    })
      .overrideProvider(HttpClient, { useValue: http });

    service = TestBed.get(EventsService);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAllEvents should', () => {

    const spy = jest.spyOn(http, 'get');

    service.getAllEvents();

    expect(spy).toHaveBeenCalledWith(`${CONFIG.API_DOMAIN_NAME}/events`);
  });

  it('createNewEvent should', () => {
    const event = {};
    const spy = jest.spyOn(http, 'post');

    service.createNewEvent(event);

    expect(spy).toHaveBeenCalledWith(`${CONFIG.API_DOMAIN_NAME}/events`, event);
  });

  it('editEvent should ', () => {
    const event = {};
    const spy = jest.spyOn(http, 'put');

    service.editEvent(event);

    expect(spy).toHaveBeenCalledWith(`${CONFIG.API_DOMAIN_NAME}/events`, event);
  });

  it('deleteEvent should ', () => {
    const eventId = '';
    const spy = jest.spyOn(http, 'delete');

    service.deleteEvent(eventId);

    expect(spy).toHaveBeenCalledWith(`${CONFIG.API_DOMAIN_NAME}/events?id=${eventId}`);
  });
});
