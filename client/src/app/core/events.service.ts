import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../common/config';
import { SportEvent } from '../common/event.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private readonly httpService: HttpClient) { }

  getAllEvents(): Observable<SportEvent[]> {
    return this.httpService.get<SportEvent[]>(`${CONFIG.API_DOMAIN_NAME}/events`);
  }

  createNewEvent(event): Observable<SportEvent> {
    return this.httpService.post<SportEvent>(`${CONFIG.API_DOMAIN_NAME}/events`, event);
  }

  editEvent(event): Observable<SportEvent> {
    return this.httpService.put<SportEvent>(`${CONFIG.API_DOMAIN_NAME}/events`, event);
  }

  deleteEvent(eventId): Observable<{ msg: string }> {
    return this.httpService.delete<{ msg: string }>(`${CONFIG.API_DOMAIN_NAME}/events?id=${eventId}`);
  }
}
